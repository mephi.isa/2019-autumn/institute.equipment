﻿using System;
using System.Collections.Generic;
using System.Text;
using Equipment;

namespace Equipment
{
    public class Room
    {
        private string number;
        private User responsible;
        private List<Equipment> listOfEquipment;
        private List<Furniture> listOfFurniture;

        public Room(string n, User r, List<Equipment> e, List<Furniture> f)
        {
            this.number = n;
            this.responsible = r;
            this.listOfEquipment = e;
            this.listOfFurniture = f;
        }

        public string getNumber()
        {
            return this.number;
        }

        public User getResponsible()
        {
            return this.responsible;
        }

        public void setResponsible(User r)
        {
            this.responsible = r;
        }

        public List<Equipment> getEquipment()
        {
            return this.listOfEquipment;
        }

        public Equipment findEquipment(string inv)
        {
            Equipment needed_element = null;
            foreach (Equipment element in this.getEquipment())
            {
                if (element.getInv() == inv)
                {
                    needed_element =  element;
                    break;
                }
            }
            return needed_element;
        }

        public string setEquipment(List<Equipment> e)
        {
            int all_seats = 0;
            int req_seats = e.Count;
            foreach (Furniture element_furniture in this.getFurniture())
            {
                all_seats += element_furniture.getSeats();
            }
            if (req_seats <= all_seats)
            {
                this.listOfEquipment = e;
                return "Оборудование добавлено";
            }
            else
            {
                return "Нет места";
            }

        }

        public string addEquipment(List<Equipment> e)
        {
            int all_seats = 0;
            int all_equipments = this.getEquipment().Count;
            int req_seats = e.Count;
            foreach (Furniture element_furniture in this.getFurniture())
            {
                all_seats += element_furniture.getSeats();
            }
            if ((all_equipments+req_seats)<=all_seats)
            {
                foreach (Equipment element in e)
                {
                    this.listOfEquipment.Add(element);
                }
                return "Оборудование добавлено";
            }
            else
            {
                return "Нет места";
            }
        }

        public void deleteEquipment(List<Equipment> e)
        {
            foreach (Equipment element in e)
            {
                this.listOfEquipment.Remove(element);
            }        
        }

        public void truncEquipment()
        {
            this.listOfEquipment.Clear();
        }

        public List<Furniture> getFurniture()
        {
            return this.listOfFurniture;
        }

        public Furniture findFurniture(string inv)
        {
            Furniture needed_element = null;
            foreach (Furniture element in this.getFurniture())
            {
                if (element.getInv() == inv)
                {
                    needed_element = element;
                    break;
                }
            }
            return needed_element;
        }

        public string setFurniture(List<Furniture> f)
        {
            int req_seats = this.getEquipment().Count;
            int new_seats = 0;
            foreach (Furniture element in f)
            {
                new_seats += element.getSeats();
            }
            if (new_seats >= req_seats)
            {
                this.listOfFurniture = f;
                return "Мебель успешно установлена в помещении";
            }
            else
            {
                return "В помещении имеется оборудование";
            }
        }

        public void addFurniture(List<Furniture> f)
        {
            foreach (Furniture element in f)
            {
                this.listOfFurniture.Add(element);
            }
        }

        public string deleteFurniture(List<Furniture> f)
        {
            int req_seats = this.getEquipment().Count;
            int cur_seats = this.getSeats();
            int del_seats = 0;
            foreach (Furniture element in f)
            {
                del_seats += element.getSeats();
            }
            if ((cur_seats - del_seats) < req_seats)
            {
                foreach (Furniture element in f)
                {
                    this.listOfFurniture.Remove(element);
                }
                return "Мебель удалена из помещения";
            }
            else
            {
                return "В комнате имеется оборудование";
            }
        }

        public string truncFurniture()
        {
            int req_seats = this.getEquipment().Count;
            if (req_seats == 0)
            {
                this.listOfFurniture.Clear();
                return "Мебель удалена из помещения";
            }
            else
            {
                return "В комнате имеется оборудование";
            }
        }

        public int getSeats()
        {
            int i = 0;
            foreach (Furniture furniture in this.listOfFurniture)
            {
                i = i + furniture.getSeats();
            }
            return i;
        }

        public List<Equipment> getFailedEquipment()
        {
            List<Equipment> i = new List<Equipment>() { };
            foreach (Equipment equipment in this.listOfEquipment)
            {
                if (equipment.getStatus() == 0)
                {
                    i.Add(equipment);
                }
            }
            return i;
        }
    }
}
