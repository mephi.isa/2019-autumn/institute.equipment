﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Equipment
{
    public class Furniture
    {
        private string name;
        private string inv;
        private int seats;

        public Furniture(string n, string i, int s)
        {
            this.name = n;
            this.inv = i;
            this.seats = s;
        }

        public string getName()
        {
            return this.name;
        }

        public void setName(string n)
        {
            this.name = n;
        }

        public string getInv()
        {
            return this.inv;
        }

        public void setInv(string i)
        {
            this.inv = i;
        }

        public int getSeats()
        {
            return this.seats;
        }

        public void setSeats(int s)
        {
            this.seats = s;
        }

    }
}
