﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Equipment
{
    public class Issue
    {
        private string key;
        private string summary;
        private string status;
        private DateTime date;
        private Equipment equipment;

        public Issue(string k, string sum, string s,DateTime d, Equipment e)
        {
            this.key = k;
            this.summary = sum;
            this.status = s;
            this.date = d;
            this.equipment = e;
        }

        public string getKey()
        {
            return this.key;
        }

        public string getSummary()
        {
            return this.summary;
        }

        public void setSummary(string s)
        {
            this.summary = s; 
        }

        public string getStatus()
        {
            return this.status;
        }

        public void setStatus(string s)
        {
            this.status = s;
        }

        public Equipment getEquipment()
        {
            return this.equipment;
        }

        public void setEquipment(Equipment e)
        {
            this.equipment = e;
        }
    }
}
