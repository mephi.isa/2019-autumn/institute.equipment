﻿using System;

namespace Equipment
{
    public class User
    {
        private string name;
        private string surname;
        private string lastname;
        private int role;
        private string department;
        private string username;

        public User(string n,string s,string l,int r,string d, string u) 
        {
            this.name = n;
            this.surname = s;
            this.lastname = l;
            this.role = r;
            this.department = d;
            this.username = u;
        }

        public string getName()
        {
            return name;
        }

        public void setName(string n)
        {
            this.name = n;
        }

        public string getSurname()
        {
            return this.surname;
        }

        public void setSurname(string s)
        {
            this.surname = s;
        }

        public string getLastname()
        {
            return this.lastname;
        }

        public void setLastname(string l)
        {
            this.lastname = l;
        }

        public int getRole()
        {
            return this.role;
        }

        public void setRole(int r)
        {
            this.role = r;
        }

        public string getDepartment()
        {
            return this.department;
        }

        public void setDepartment(string d)
        {
            this.department = d;
        }

        public string getUsername()
        {
            return this.username;
        }

        public void setUsername(string u)
        {
            this.username = u;
        }

        public User deleteUser()
        {
            GC.SuppressFinalize(this);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            return null;
        }
    }
}
