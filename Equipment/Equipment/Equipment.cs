﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Equipment
{
    public class Equipment
    {
        private string name;
        private string inv;
        private int status;

        public Equipment(string n,string i,int s)
        {
            this.name = n;
            this.inv = i;
            this.status = s;
        }

        public string getName()
        {
            return this.name;
        }

        public void setName(string n)
        {
            this.name = n;
        }

        public string getInv()
        {
            return this.inv;
        }

        public void setInv(string i)
        {
            this.inv = i;
        }

        public int getStatus()
        {
            return this.status;
        }

        public void setStatus(int s)
        {
            this.status = s;
        }
    }
}
