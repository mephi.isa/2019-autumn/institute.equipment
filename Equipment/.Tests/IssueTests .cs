﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Equipment;
using System;
using System.Collections.Generic;
using System.Text;

namespace Equipment.Tests
{
    [TestClass()]
    public class IssueTests
    {
        static DateTime datetime = new DateTime(2019,09,12,12,00,00);
        static Equipment e = new Equipment("Компьютер А", "1", 0);
        static Issue i = new Issue("ERR-1", "Поломка", "Зарегистрировано", datetime, e);
        [TestMethod()]
        public void getNameTest()
        {
            //arrange
            string expected = "ERR-1";
            //act
            string actual = i.getKey();
            //assert
            Assert.AreEqual(expected,actual);
        }
        
        [TestMethod()]
        public void getSummaryTest()
        {
            //arrange
            string expected = "Поломка";
            //act
            string actual = i.getSummary();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setSummaryTest()
        {
            //arrange
            string expected = "Сгорел";
            //act
            i.setSummary("Сгорел");
            string actual = i.getSummary();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getStatusTest()
        {
            //arrange
            string expected = "Зарегистрировано";
            //act
            string actual = i.getStatus();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setStatusTest()
        {
            //arrange
            string expected = "В работе";
            //act
            i.setStatus("В работе");
            string actual = i.getStatus();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getEquipmentTest()
        {
            //arrange
            Equipment expected = e;
            //act
            Equipment actual = i.getEquipment();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setEquipmentTest()
        {
            //arrange
            Equipment expected = new Equipment("Компьютер Б", "2", 0);
            //act
            i.setEquipment(expected);
            Equipment actual = i.getEquipment();
            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}