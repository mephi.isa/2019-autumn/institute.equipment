﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Equipment;
using System;
using System.Collections.Generic;
using System.Text;

namespace Equipment.Tests
{
    [TestClass()]
    public class EquipmentTests
    {
        public Equipment e = new Equipment("Компьютер А", "1", 1);
        [TestMethod()]
        public void getNameTest()
        {
            //arrange
            string expected = "Компьютер А";
            //act
            string actual = e.getName();
            //assert
            Assert.AreEqual(expected,actual);
        }

        [TestMethod()]
        public void setNameTest()
        {
            //arrange
            string expected = "Компьютер Б";
            //act
            e.setName("Компьютер Б");
            string actual = e.getName();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getInvTest()
        {
            //arrange
            string expected = "1";
            //act
            string actual = e.getInv();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setInvTest()
        {
            //arrange
            string expected = "2";
            //act
            e.setInv("2");
            string actual = e.getInv();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getStatusTest()
        {
            //arrange
            int expected = 1;
            //act
            int actual = e.getStatus();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setStatusTest()
        {
            //arrange
            int expected = 2;
            //act
            e.setStatus(2);
            int actual = e.getStatus();
            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}