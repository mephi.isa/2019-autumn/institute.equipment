﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Equipment;
using System;
using System.Collections.Generic;
using System.Text;

namespace Equipment.Tests
{
    [TestClass()]
    public class FurnitureTests
    {
        public Furniture f = new Furniture("Стол", "1", 1);
        [TestMethod()]
        public void getNameTest()
        {
            //arrange
            string expected = "Стол";
            //act
            string actual = f.getName();
            //assert
            Assert.AreEqual(expected,actual);
        }

        [TestMethod()]
        public void setNameTest()
        {
            //arrange
            string expected = "Стол 2 типа";
            //act
            f.setName("Стол 2 типа");
            string actual = f.getName();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getInvTest()
        {
            //arrange
            string expected = "1";
            //act
            string actual = f.getInv();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setInvTest()
        {
            //arrange
            string expected = "2";
            //act
            f.setInv("2");
            string actual = f.getInv();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getSeatsTest()
        {
            //arrange
            int expected = 1;
            //act
            int actual = f.getSeats();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setSeatsTest()
        {
            //arrange
            int expected = 2;
            //act
            f.setSeats(2);
            int actual = f.getSeats();
            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}