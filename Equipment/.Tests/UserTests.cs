﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Equipment;
using System;
using System.Collections.Generic;
using System.Text;

namespace Equipment.Tests
{
    [TestClass()]
    public class UserTests
    {
        public User u = new User("Иван", "Иванов", "Иванович", 1, "Отдел А", "testuser");
        [TestMethod()]
        public void getNameTest()
        {
            //arrange
            string expected = "Иван";
            //act
            string actual = u.getName();
            //assert
            Assert.AreEqual(expected,actual);
        }

        [TestMethod()]
        public void setNameTest()
        {
            //arrange
            string expected = "Иван2";
            //act
            u.setName("Иван2");
            string actual = u.getName();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getSurnameTest()
        {
            //arrange
            string expected = "Иванов";
            //act
            string actual = u.getSurname();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setSurnameTest()
        {
            //arrange
            string expected = "Иванов2";
            //act
            u.setSurname("Иванов2");
            string actual = u.getSurname();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getLastnameTest()
        {
            //arrange
            string expected = "Иванович";
            //act
            string actual = u.getLastname();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setLastnameTest()
        {
            //arrange
            string expected = "Иванович2";
            //act
            u.setLastname("Иванович2");
            string actual = u.getLastname();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getRoleTest()
        {
            //arrange
            int expected = 1;
            //act
            int actual = u.getRole();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setRoleTest()
        {
            //arrange
            int expected = 2;
            //act
            u.setRole(2);
            int actual = u.getRole();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getDepartmentTest()
        {
            //arrange
            string expected = "Отдел А";
            //act
            string actual = u.getDepartment();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setDepartmentTest()
        {
            //arrange
            string expected = "Отдел Б";
            //act
            u.setDepartment("Отдел Б");
            string actual = u.getDepartment();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getUsernameTest()
        {
            //arrange
            string expected = "testuser";
            //act
            string actual = u.getUsername();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setUsernameTest()
        {
            //arrange
            string expected = "testuser2";
            //act
            u.setUsername("testuser2");
            string actual = u.getUsername();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void deleteUserTest()
        {
            //arrange
            User expected = null;
            //act
            User actual = u.deleteUser();
            //assert
            Assert.AreEqual(expected, actual);
        }

    }
}