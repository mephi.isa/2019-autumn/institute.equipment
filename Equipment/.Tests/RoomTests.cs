﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Equipment;
using System;
using System.Collections.Generic;
using System.Text;

namespace Equipment.Tests
{
    [TestClass()]
    public class RoomTests
    {
        static User u = new User("Иван", "Иванов", "Иванович", 1, "Отдел А", "ivanovich");
        static Equipment e1 = new Equipment("Компьютер А", "1", 1);
        static Equipment e2 = new Equipment("Компьютер Б", "2", 1);
        static Equipment e3 = new Equipment("Компьютер В", "3", 0);
        static List<Equipment> loe = new List<Equipment>() { e1, e2, e3 };
        static Furniture f1 = new Furniture("Стол А", "4", 1);
        static Furniture f2 = new Furniture("Стол Б", "5", 1);
        static Furniture f3 = new Furniture("Стол В", "6", 1);
        static Furniture f4 = new Furniture("Стол Г", "7", 1);
        static List<Furniture> lof = new List<Furniture>() { f1, f2, f3, f4 };
        static Room r = new Room("Г-101", u, loe, lof);

        private void set_default_vars() {
            User u = new User("Иван", "Иванов", "Иванович", 1, "Отдел А", "ivanovich");
            Equipment e1 = new Equipment("Компьютер А", "1", 1);
            Equipment e2 = new Equipment("Компьютер Б", "2", 1);
            Equipment e3 = new Equipment("Компьютер В", "3", 0);
            List<Equipment> loe = new List<Equipment>() { e1, e2, e3 };
            Furniture f1 = new Furniture("Стол А", "4", 1);
            Furniture f2 = new Furniture("Стол Б", "5", 1);
            Furniture f3 = new Furniture("Стол В", "6", 1);
            Furniture f4 = new Furniture("Стол Г", "7", 1);
            List<Furniture> lof = new List<Furniture>() { f1, f2, f3, f4 };
            Room r = new Room("Г-101", u, loe, lof);
        }
        [TestMethod()]
        public void getNumberTest()
        {
            //arrange
            set_default_vars();
            string expected = "Г-101";
            //act
            string actual = r.getNumber();
            //assert
            Assert.AreEqual(expected,actual);
        }

        [TestMethod()]
        public void getResponsibleTest()
        {
            //arrange
            set_default_vars();
            User expected = u;
            //act
            User actual = r.getResponsible();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setResponsibleTest()
        {
            //arrange
            set_default_vars();
            User expected = new User("Петр","Петров","Петрович",2,"Отдел Б","petrovich");
            //act
            r.setResponsible(expected);
            User actual = r.getResponsible();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getEquipmentTest()
        {
            //arrange
            set_default_vars();
            List<Equipment> expected = loe;
            //act
            List<Equipment> actual = r.getEquipment();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void findipmentTest()
        {
            //arrange
            set_default_vars();
            Equipment expected = e1;
            //act
            Equipment actual = r.findEquipment("1");
            //assert
            Assert.ReferenceEquals(expected, actual);
        }

        [TestMethod()]
        public void setEquipmentTest()
        {
            //arrange
            set_default_vars();
            Equipment e4 = new Equipment("Компьютер Г", "8", 1);
            List<Equipment> expected2 = new List<Equipment>() {e4,e1};
            string expected1 = "Оборудование добавлено";
            
            //act
            string actual1 = r.setEquipment(expected2);
            List<Equipment> actual2 = r.getEquipment();
            //assert
            Assert.AreEqual(expected1, actual1);
            Assert.ReferenceEquals(expected2, actual2);
        }

        [TestMethod()]
        public void addEquipmentTest()
        {
            //arrange
            set_default_vars();
            Equipment e4 = new Equipment("Компьютер Г", "8", 1);
            List<Equipment> toadd = new List<Equipment>() { e4 };
            List<Equipment> expected2 = new List<Equipment>() { e1, e2, e3, e4 };
            string expected1 = "Оборудование добавлено";

            //act
            string actual1 = r.addEquipment(toadd);
            List<Equipment> actual2 = r.getEquipment();
            //assert
            Assert.AreEqual(expected1, actual1);
            Assert.ReferenceEquals(expected2, actual2);
        }

        [TestMethod()]
        public void deleteEquipmentTest()
        {
            //arrange
            set_default_vars();
            List<Equipment> expected = new List<Equipment>() { e2, e3 };
            List<Equipment> todelete = new List<Equipment>() { e1 };
            //act
            r.deleteEquipment(todelete);
            List<Equipment> actual = r.getEquipment();
            //assert
            Assert.ReferenceEquals(expected, actual);
        }

        [TestMethod()]
        public void truncEquipmentTest()
        {
            //arrange
            set_default_vars();
            List<Equipment> expected = new List<Equipment>() { };
            //act
            r.truncEquipment();
            List<Equipment> actual = r.getEquipment();
            //assert
            Assert.ReferenceEquals(expected, actual);
        }

        [TestMethod()]
        public void getFurnitureTest()
        {
            //arrange
            set_default_vars();
            List<Furniture> expected = lof;
            //act
            List<Furniture> actual = r.getFurniture();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void findFurnitureTest()
        {
            //arrange
            set_default_vars();
            Furniture expected = f1;
            //act
            Furniture actual = r.findFurniture("4");
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setFurnitureTest()
        {
            //arrange
            set_default_vars();
            Furniture f5 = new Furniture("Стол Д", "9", 1);
            List<Furniture> expected2 = new List<Furniture>() { f1, f2, f3, f4, f5 };
            string expected1 = "Мебель успешно установлена в помещении";

            //act
            string actual1 = r.setFurniture(expected2);
            List<Furniture> actual2 = r.getFurniture();
            //assert
            Assert.AreEqual(expected1, actual1);
            Assert.AreEqual(expected2, actual2);
        }

        [TestMethod()]
        public void addFurnitureTest()
        {
            //arrange
            set_default_vars();
            Furniture f5 = new Furniture("Стол Д", "9", 1);
            List<Furniture> toadd = new List<Furniture>() { f5 };
            List<Furniture> expected = new List<Furniture>() { f1, f2, f3, f4, f5 };

            //act
            r.addFurniture(toadd);
            List<Furniture> actual = r.getFurniture();
            //assert
            Assert.ReferenceEquals(expected, actual);
        }

        [TestMethod()]
        public void deleteFurnitureTest()
        {
            //arrange
            set_default_vars();
            List<Furniture> expected = new List<Furniture>() { f2, f3, f4 };
            List<Furniture> todelete = new List<Furniture>() { f1 };
            //act
            r.deleteFurniture(todelete);
            List<Furniture> actual = r.getFurniture();
            //assert
            Assert.ReferenceEquals(expected, actual);
        }

        [TestMethod()]
        public void truncFurnitureTest()
        {
            //arrange
            set_default_vars();
            List<Furniture> expected = new List<Furniture>() { };
            //act
            r.truncFurniture();
            List<Furniture> actual = r.getFurniture();
            //assert
            Assert.ReferenceEquals(expected, actual);
        }

        [TestMethod()]
        public void getSeatsTest()
        {
            //arrange
            set_default_vars();
            int expected = 4;
            //act
            int actual = r.getSeats();
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getFailedEquipmentTest()
        {
            //arrange
            set_default_vars();
            List<Equipment> expected = new List<Equipment>() { e3 };
            //act
            List<Equipment> actual = r.getFailedEquipment();
            //assert
            Assert.ReferenceEquals(expected, actual);
        }


    }
}